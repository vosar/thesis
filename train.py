from __future__ import print_function, absolute_import

import os
import errno
import time

import numpy as np
import cv2
import torch
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim

from tensorboardX import SummaryWriter

from evaluation import AverageMeter, iou_loss
from utils.misc import save_checkpoint, adjust_learning_rate
from dataset import LandmarkDataset
from options.train_options import TrainOptions

from tqdm import tqdm

from models.unet_mobilenet import UNetMobileNet
from models.unet_resnet import UNetResNet
from models.unet_seresnext import UNetSEResNeXt

best_acc = 0
best_loss = 1000000.0


def main(args):
    global best_acc
    global best_loss

    # create resnet101 dir
    if not os.path.isdir(os.path.join("checkpoints", args.name)):
        try:
            os.makedirs(os.path.join("checkpoints", args.name))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    # create model
    if args.model_architecture == 'unet_resnet':
        model = UNetResNet(num_classes=args.num_classes, encoder_depth=args.resnet_size)
    elif args.model_architecture == 'unet_mobilenet':
        model = UNetMobileNet(num_classes=args.num_classes, pre_trained="../trained_models/mobilenet_v2.pth")
    elif args.model_architecture == 'unet_seresnext':
        model = UNetSEResNeXt(num_classes=args.num_classes, pretrained=True)
    else:
        raise Exception(f"{args.model_architecture} model architecture is not available!")
    model = torch.nn.DataParallel(model).cuda()

    criterion = iou_loss

    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=args.lr)

    # optionally resume from a resnet101
    title = 'keypoints_hg'
    if args.resume:
        if args.resume and os.path.isdir(os.path.join("checkpoints", args.name)):
            print("=> loading '{}'".format(args.name))
            path = os.path.join("checkpoints", args.name, "checkpoint.pth.tar")
            checkpoint = torch.load(path)
            args.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_acc']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded '{}' (epoch {})"
                  .format(args.name, checkpoint['epoch']))
        else:
            print("=> no model found at '{}'".format(args.resume))

    cudnn.benchmark = True
    print('    Total params: %.2fM' % (sum(p.numel() for p in model.parameters()) / 1000000.0))

    # Data loading code
    train_loader = torch.utils.data.DataLoader(
        LandmarkDataset(args.dataroot, out_res=(args.load_size, args.load_size),  train=True),
        batch_size=args.train_batch, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        LandmarkDataset(args.dataroot, out_res=(args.load_size, args.load_size), train=False),
        batch_size=args.test_batch, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    lr = args.lr

    save_path = "./logs/{}".format(args.name)
    logger = SummaryWriter(save_path)

    for epoch in range(args.start_epoch, args.epochs):
        lr = adjust_learning_rate(optimizer, epoch, lr, args.schedule, args.gamma)
        print('\nEpoch: %d | LR: %.8f' % (epoch + 1, lr))

        # decay sigma
        if args.sigma_decay > 0:
            train_loader.dataset.sigma *= args.sigma_decay
            val_loader.dataset.sigma *= args.sigma_decay

        # train for one epoch
        train_loss = train(train_loader, model, criterion, optimizer, logger, epoch, args.debug, args.flip)

        # evaluate on validation set
        valid_loss = validate(val_loader, model, criterion, args.num_classes, logger, epoch,
                              args.debug, args.flip)

        logger.add_scalar('train_loss', train_loss, epoch)
        logger.add_scalar('val_loss', valid_loss, epoch)

        # remember best acc and save resnet101
        #         is_best = valid_acc > best_acc
        #         best_acc = max(valid_acc, best_acc)
        is_best = valid_loss < best_loss
        best_loss = min(valid_loss, best_loss)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_acc': best_loss,
            'optimizer': optimizer.state_dict(),
        }, is_best, checkpoint=os.path.join("checkpoints", args.name), snapshot=20)


def train(train_loader, model, criterion, optimizer, logger, epoch, debug=False, flip=True):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    acces = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()

    gt_win, pred_win = None, None

    for inputs, target, meta in tqdm(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        input_var = inputs.cuda()
        target_var = target.cuda()

        # compute output
        output = model(input_var)
        score_map = output.data.cpu()

        # print(target_var)
        loss = criterion(output, target_var)

        # print(loss.item())

        # measure accuracy and record loss
        losses.update(loss.item(), inputs.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    return losses.avg


def validate(val_loader, model, criterion, num_classes, logger, epoch, debug=False, flip=True):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()

    # switch to evaluate mode
    model.eval()

    gt_win, pred_win = None, None
    end = time.time()

    for i, (inputs, target, meta) in enumerate(val_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        target_var = target.cuda()
        input_var = inputs.cuda()
        # target_var = torch.autograd.Variable(target)

        # compute output
        with torch.no_grad():
            output = model(input_var)
            score_map = output.data.cpu()
        if i == 0:
            # disp_pred = inputs[0]
            # for i in range(target.shape[1]):
            #     disp_pred -= score_map[0][i]
            inp_img = inputs.detach().cpu().numpy()[0]
            # pts = np.zeros((model.num_classes, 2))
            for j in range(args.num_classes):
                point = np.array(np.unravel_index(score_map[0][j].argmax(), score_map[0][j].shape)[::-1])
                cv2.circle(inp_img, tuple(point), 3, (0,255,0), -1)
                # pts[i] = point
            logger.add_image('Prediction', inp_img, epoch)

        loss = criterion(output, target_var)

        # measure accuracy and record loss
        losses.update(loss.item(), inputs.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    return losses.avg


if __name__ == '__main__':
    args = TrainOptions().parse()
    main(args)
