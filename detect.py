from utils.utils import *
from utils.image_utils import im_to_torch
from albumentations import Compose, Normalize
import torch
import matplotlib.pyplot as plt
import time
import numpy as np
import cv2
from face_recognition import face_landmarks
from models.unet_resnet import UNetResNet
from glob import glob
import pandas as pd
import skimage
from utils.data import anno_to_numpy
from tqdm import tqdm
import math

# import dlib
# from imutils import face_utils

frames_to_process = 300
chunk_frame_cnt = 900

HEIGHT = 256
WIDTH = 256
CHANNELS = 3
NUM_LANDMARKS = 56


# def get_five_landmarks(img):
#     landmarks = face_landmarks(img)[0]
#     landmarks = [landmarks["left_eye"][0], landmarks["right_eye"][3], landmarks["nose_bridge"][-1], landmarks["top_lip"][0], landmarks["bottom_lip"][0]]
#     return np.array(landmarks, dtype=np.float64)


def lands_list_to_dict(landmarks):
    return {
        'top_lip': np.concatenate((landmarks[34:41], landmarks[47:51]), axis=None),
        'bottom_lip': np.concatenate((landmarks[41:47], landmarks[51:54]), axis=None),
        'left_eye': landmarks[18:26],
        'right_eye': landmarks[26:34],
        'left_eyebrow': landmarks[9:18],
        'right_eyebrow': landmarks[0:9],
    }


def get_dlib_landmarks(face):
    # predictor = dlib.shape_predictor(weights)
    # gray = cv2.cvtColor(face, cv2.COLOR_BGR2GRAY)
    # shape = predictor(gray, rect)
    # shape = face_utils.shape_to_np(shape)
    landmarks = face_landmarks(face)[0]
    landmarks.pop('chin', None)
    landmarks.pop('nose_bridge', None)
    landmarks.pop('nose_tip', None)
    print(len(landmarks['top_lip']))
    print(len(landmarks['bottom_lip']))
    return np.array(landmarks['left_eyebrow'] + landmarks['right_eyebrow'] + landmarks['left_eye'] + landmarks['right_eye'] + \
           landmarks['top_lip'] + landmarks['bottom_lip'])


def get_landmarks(face, model, transform):
    input_height, input_width = face.shape[:2]
    scale_height, scale_width = input_height / HEIGHT, input_width / WIDTH
    resized = cv2.resize(face, (WIDTH, HEIGHT))
    augmented = transform(image=resized)
    inp = im_to_torch(augmented["image"]).reshape(1, CHANNELS, HEIGHT, WIDTH)
    # input_var = torch.autograd.Variable(inp.cuda(), requires_grad=False)
    with torch.no_grad():
        output = model(inp.cuda())
        score_map = output.data.cpu()
    pts = np.zeros((model.num_classes, 2))
    for i in range(0, model.num_classes):
        point = np.array(np.unravel_index(score_map[0][i].argmax(), score_map[0][i].shape)[::-1])
        pts[i] = point
    pts *= np.array([scale_height, scale_width])
    return pts


def landmark_impainting(model, image, bounding_box, transform):
    ((face_y, face_yh), (face_x, face_xw)) = bounding_box
    face = image[face_y:face_yh, face_x:face_xw]
    pts = get_landmarks(face, model, transform)
    pts = list(filter(lambda x: x[1] > 300, pts))
    face_with_landmarks = face.copy()
    for point in pts:
        face_with_landmarks = cv2.circle(face_with_landmarks, (int(point[0]), int(point[1])), 1, (0, 0, 255), -1)

    image[face_y:face_yh, face_x:face_xw] = face_with_landmarks
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    return image


def process_video(path, weights, transform):
    cap_avatar = cv2.VideoCapture(path)
    result_frames = []
    frame_index = 0
    while (cap_avatar.isOpened() and frame_index < frames_to_process):
        if (frames_to_process - frame_index) % 10 == 0:
            print(frames_to_process - frame_index)

        ret, frame_avatar = cap_avatar.read()
        result_frame = frame_avatar
        bounding_box = find_face(frame_avatar)
        if bounding_box:
            start = time.time()
            result_frame = landmark_impainting(weights, frame_avatar, bounding_box, transform)
            print("Prediction time: ", time.time() - start)
        result_frames.append(result_frame)
        frame_index += 1
        plt.imshow(result_frame)
        plt.show()
        exit(0)

    # write_video(result_frames, "result_0.mov")


def process_image_folder(anno_path, weights, transform):
    anno = pd.read_csv(anno_path).values
    errors = []
    for sample in tqdm(anno):
        target_lands = sample[:-1].reshape((-1, 2))

        img_path = sample[-1]  # '\\'.join(sample[-1].split('/'))
        try:
            img = cv2.imread(os.path.join('/'.join(anno_path.split("/")[:-1]), img_path))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            # dlib_lands = get_dlib_landmarks(img)
            # print(len(dlib_lands))
            unet_lands = get_landmarks(img, weights, transform)
            # show_landmarks(img, target_lands)
            # print(target_lands[34:54])
            # print(dlib_lands)
            # show_landmarks(img, dlib_lands)
            # show_landmarks(img, unet_lands)
            # exit(0)
        except Exception:
            print(img_path)
            continue
        land_error_sum = 0
        for i in range(len(target_lands)):
            # print('target', target_lands[i])
            # print('unet', unet_lands[i])
            # exit(0)
            land_error = math.sqrt(
                (target_lands[i][0] - unet_lands[i][0]) ** 2 + (target_lands[i][1] - unet_lands[i][1]) ** 2)
            land_error_sum += land_error
            # print(land_error)

        # print(error)
        errors.append(land_error_sum / len(target_lands))
    return np.mean(errors)


if __name__ == "__main__":
    WEIGHTS_PATH = 'weights/unet_resnet18_150epoch.pth'
    VIDEO_PATH = "../../raw_data/in_videos/warden_videos/A006_C007_01014E.mp4"
    IMG_ANNO_PATH = "datasets/data_new/val/annotations.csv"
    IMG_ANNO_PATH_NEW = "datasets/wflw_reduced/train_5k_val_3k/test/annotations.csv"

    trans = Compose([
        Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225],
        ),
    ])

    model = UNetResNet(num_classes=56, encoder_depth=18)
    model = load_model(model, WEIGHTS_PATH)
    model.eval()
    model.to('cuda')
    # img = cv2.imread('data_fr\\01_110_2527.jpg')
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #
    # unet_lands = get_landmarks(img, model, trans)
    # # show_landmarks(img, target_lands)
    # show_landmarks(img, unet_lands)

    mean_error = process_image_folder(IMG_ANNO_PATH_NEW, model, trans)
    print(mean_error)
#3.6142334919873984