import logging
import math
import sys

import torch
import torch.nn as nn
from torch.nn import functional as F

from models.mobilenet import MobileNetV2, InvertedResidual


def conv3x3(in_, out):
    return nn.Conv2d(in_, out, 3, padding=1)


class ConvRelu(nn.Module):
    def __init__(self, in_, out):
        super(ConvRelu, self).__init__()
        self.conv = conv3x3(in_, out)
        self.activation = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.activation(x)
        return x


class UNetMobileNet(nn.Module):
    def __init__(self, num_classes, pre_trained='../trained_models/mobilenet_v2.pth'):
        super(UNetMobileNet, self).__init__()
        self.num_classes = num_classes
        self.dropout_2d = 0.2

        self.backbone = MobileNetV2()

        self.dconv1 = nn.ConvTranspose2d(1280, 96, 4, padding=1, stride=2)
        self.invres1 = InvertedResidual(192, 96, 1, 6)

        self.dconv2 = nn.ConvTranspose2d(96, 32, 4, padding=1, stride=2)
        self.invres2 = InvertedResidual(64, 32, 1, 6)

        self.dconv3 = nn.ConvTranspose2d(32, 24, 4, padding=1, stride=2)
        self.invres3 = InvertedResidual(48, 24, 1, 6)

        self.dconv4 = nn.ConvTranspose2d(24, 16, 4, padding=1, stride=2)
        self.invres4 = InvertedResidual(32, 16, 1, 6)

        self.dconv5 = ConvRelu(168, 64)

        #self.conv_last = nn.Conv2d(16, 3, 1)

        #self.conv_score = nn.Conv2d(3, 1, 1)
        self.final = nn.Conv2d(64, num_classes, kernel_size=1)

        self._init_weights()

        if pre_trained is not None:
            self.backbone.load_state_dict(torch.load(pre_trained))

    def forward(self, x):
        for n in range(0, 2):
            x = self.backbone.features[n](x)
        x1 = x
        logging.debug((x1.shape, 'x1'))

        for n in range(2, 4):
            x = self.backbone.features[n](x)
        x2 = x
        logging.debug((x2.shape, 'x2'))

        for n in range(4, 7):
            x = self.backbone.features[n](x)
        x3 = x
        logging.debug((x3.shape, 'x3'))

        for n in range(7, 14):
            x = self.backbone.features[n](x)
        x4 = x
        logging.debug((x4.shape, 'x4'))

        for n in range(14, 19):
            x = self.backbone.features[n](x)
        x5 = x
        logging.debug((x5.shape, 'x5'))

        up1 = torch.cat([
            x4,
            self.dconv1(x)
        ], dim=1)
        up1 = self.invres1(up1)
        logging.debug((up1.shape, 'up1'))

        up2 = torch.cat([
            x3,
            self.dconv2(up1)
        ], dim=1)
        up2 = self.invres2(up2)
        logging.debug((up2.shape, 'up2'))

        up3 = torch.cat([
            x2,
            self.dconv3(up2)
        ], dim=1)
        up3 = self.invres3(up3)
        logging.debug((up3.shape, 'up3'))

        up4 = torch.cat([
            x1,
            self.dconv4(up3)
        ], dim=1)
        up4 = self.invres4(up4)
        logging.debug((up4.shape, 'up4'))

        f = torch.cat((
            #up4,
            F.interpolate(up4, scale_factor=2, mode='bilinear', align_corners=False),
            F.interpolate(up3, scale_factor=4, mode='bilinear', align_corners=False),
            F.interpolate(up2, scale_factor=8, mode='bilinear', align_corners=False),
            F.interpolate(up1, scale_factor=16, mode='bilinear', align_corners=False),
        ), 1)
        logging.debug((f.shape, 'f'))

        up5 = self.dconv5(F.dropout2d(f, p=self.dropout_2d))
        logging.debug((up5.shape, "up5"))

        #x = self.conv_last(up4)
        # logging.debug((x.shape, 'conv_last'))

        # x = self.conv_score(x)
        # logging.debug((x.shape, 'conv_score'))

        # x = interpolate(x, scale_factor=2, mode='bilinear', align_corners=False)
        # logging.debug((x.shape, 'interpolate'))

        #x = torch.sigmoid(x)

        final = self.final(up5)
        logging.debug((final.shape, "final"))

        return final

    def _init_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


if __name__ == '__main__':
    # Debug
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    net = UNetMobileNet(56, pre_trained="/home/ivankosarevych/Documents/SoftServe/Digital_Avatars/face_reenacment/trained_models/mobilenet_v2.pth")
    net(torch.randn(1, 3, 224, 224))
