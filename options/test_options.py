from .base_options import BaseOptions


class TestOptions(BaseOptions):
    def initialize(self):
        BaseOptions.initialize(self)

        self.parser.add_argument('-w', '--weights', default='', type=str,
                                 help='path to model weights')
        self.parser.add_argument('--how_many', type=int, default=5, help='how many test images to run')
        self.is_train = False
