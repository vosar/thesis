from .base_options import BaseOptions


class TrainOptions(BaseOptions):
    def initialize(self):
        BaseOptions.initialize(self)

        self.parser.add_argument('--epochs', default=200, type=int, metavar='N',
                            help='number of total epochs to run')
        self.parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                            help='manual epoch number (useful on restarts)')
        self.parser.add_argument('--train-batch', default=4, type=int, metavar='N',
                            help='train batchsize')
        self.parser.add_argument('--test-batch', default=2, type=int, metavar='N',
                            help='test batchsize')
        self.parser.add_argument('--lr', '--learning-rate', default=1e-4, type=float,
                            metavar='LR', help='initial learning rate')
        self.parser.add_argument('--momentum', default=0, type=float, metavar='M',
                            help='momentum')
        self.parser.add_argument('--weight-decay', '--wd', default=0, type=float,
                            metavar='W', help='weight decay (default: 0)')
        self.parser.add_argument('--schedule', type=int, nargs='+', default=[60, 80],
                            help='Decrease learning rate at these epochs.')
        self.parser.add_argument('--gamma', type=float, default=0.1,
                               help='LR is multiplied by gamma on schedule.')
        self.parser.add_argument('-c', '--resnet101', default='resnet101', type=str, metavar='PATH',
                                 help='path to save resnet101 (default: resnet101)')
        self.parser.add_argument('--resume', action='store_true',
                                 help='whether to resume training')
        self.is_train = True
