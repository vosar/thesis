import argparse


class BaseOptions:
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.initialized = False

    def initialize(self):
        # experiment specifics
        self.parser.add_argument('-n', '--name', default="unet_rn101_bb_2points", type=str,
                                 help='Name of the experiment')
        self.parser.add_argument('-d', '--dataroot', default="data_new/train", type=str,
                                 help='Path to data set directory')
        self.parser.add_argument('--load-size', default=256, type=int,
                                 help='Output image dimension')
        self.parser.add_argument('-m', '--model-architecture', default='unet_resnet', type=str,
                                 help='Model architecture')
        self.parser.add_argument('--resnet-size', default=34, type=int,
                                 help='Number of layers in ResNet')
        self.parser.add_argument('-s', '--stacks', default=4, type=int, metavar='N',
                            help='Number of hourglasses to stack')
        self.parser.add_argument('--features', default=256, type=int, metavar='N',
                            help='Number of features in the hourglass')
        self.parser.add_argument('-b', '--blocks', default=1, type=int, metavar='N',
                            help='Number of residual modules at each location in the hourglass')
        self.parser.add_argument('--num-classes', default=56, type=int, metavar='N',
                            help='Number of keypoints')
        # Training strategy
        self.parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                            help='number of data loading workers (default: 4)')
        # Data processing
        self.parser.add_argument('-f', '--flip', dest='flip', action='store_true',
                            help='flip the input during validation')
        self.parser.add_argument('--sigma', type=float, default=1,
                            help='Groundtruth Gaussian sigma.')
        self.parser.add_argument('--sigma-decay', type=float, default=0,
                            help='Sigma decay rate for each epoch.')
        self.parser.add_argument('--label-type', metavar='LABELTYPE', default='Gaussian',
                            choices=['Gaussian', 'Cauchy'],
                            help='Labelmap dist type: (default=Gaussian)')
        # Miscs
        self.parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
                            help='evaluate model on validation set')
        self.parser.add_argument('-db', '--debug', dest='debug', action='store_true',
                            help='show intermediate results')

        self.initialized = True

    def parse(self, save=True):
        if not self.initialized:
            self.initialize()

        self.opt = self.parser.parse_args()
        self.opt.isTrain = self.is_train   # train or test

        return self.opt
