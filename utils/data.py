import os
import pandas as pd
import numpy as np
from utils.utils import show_landmarks


def read_wflw_raw(path):
    with open(path) as f:
        for line in f:
            sample = line.split()
            img_path = sample[-1]
            print(img_path)
            lands = sample[:98*2]
            np_lands = np.array(lands).reshape((-1, 2))
            # show_landmarks()
            exit(0)


def anno_to_numpy(anno_path):
    anno = pd.read_csv(anno_path)
    lands = anno.iloc[:, :-1].values
    return lands.reshape((-1, 2))


if __name__ == "__main__":
    print(os.getcwd())
    read_wflw_raw("list_98pt_rect_attr_test.txt")
