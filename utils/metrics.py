import numpy as np


def get_nmse(landmarks_generated, landmarks_actor):
    actor_left_eye = np.array(landmarks_actor["left_eye"])
    actor_right_eye = np.array(landmarks_actor["right_eye"])

    # centroid of all eye points
    actor_left_eye = np.array(
        [np.sum(actor_left_eye[:, 0]) / len(actor_left_eye), np.sum(actor_left_eye[:, 1]) / len(actor_left_eye)])
    actor_right_eye = np.array(
        [np.sum(actor_right_eye[:, 0]) / len(actor_right_eye), np.sum(actor_right_eye[:, 1]) / len(actor_right_eye)])

    # average between left and right part of an eye
    # actor_left_eye = np.array([actor_left_eye[0][0] + (actor_left_eye[3][0] - actor_left_eye[0][0])//2, actor_left_eye[0][1] + (actor_left_eye[3][1] - actor_left_eye[0][1])//2])
    # actor_right_eye = np.array([actor_right_eye[0][0] + (actor_right_eye[3][0] - actor_right_eye[0][0])//2, actor_right_eye[0][1] + (actor_right_eye[3][1] - actor_right_eye[0][1])//2])

    # print(np.sqrt((actor_left_eye[0] - actor_right_eye[0])**2 + (actor_left_eye[1] - actor_right_eye[1])**2))

    interocular = np.linalg.norm(actor_left_eye - actor_right_eye)

    landmarks_generated_flatten = landmarks_generated["left_eyebrow"] + landmarks_generated["right_eyebrow"] + \
                                  landmarks_generated["left_eye"] + landmarks_generated["right_eye"] + \
                                  landmarks_generated["top_lip"] + landmarks_generated["bottom_lip"]
    landmarks_generated_flatten = np.array(landmarks_generated_flatten)
    landmarks_actor_flatten = landmarks_actor["left_eyebrow"] + landmarks_actor["right_eyebrow"] + landmarks_actor[
        "left_eye"] + landmarks_actor["right_eye"] + landmarks_actor["top_lip"] + landmarks_actor["bottom_lip"]
    landmarks_actor_flatten = np.array(landmarks_actor_flatten)

    # show_landmarks(generated, landmarks_generated_flatten)
    # plt.show()
    # show_landmarks(actor, d)
    # plt.show()

    error = np.sum(np.linalg.norm(landmarks_generated_flatten - landmarks_actor_flatten, axis=1)) / (
            interocular * len(landmarks_actor_flatten))
    return error
