import os
import matplotlib.pyplot as plt
from skvideo.io import vwrite
import numpy as np
import cv2
import face_recognition
import torch
from copy import deepcopy
# from models.unet_resnet import UNetResNet
# from models.unet_mobilenet import UNetMobileNet


def show_landmarks(image, landmarks):
    """Show image with landmarks"""
    plt.imshow(image)
    plt.scatter(landmarks[:, 0], landmarks[:, 1], s=15, marker='.', c='r')
    plt.pause(0.001)  # pause a bit so that plots are updated


def show_lands_with_text(image, landmarks):
    i = 0
    img_copy = deepcopy(image)
    font = cv2.FONT_HERSHEY_SIMPLEX
    for land in landmarks:
        cv2.circle(img_copy, land, 2, (0, 255, 0), -1)
        cv2.putText(img_copy, str(i), land, font, 1, (255,255,255),2,cv2.LINE_AA)
        i += 1


def write_video(output_video, filename):
    output_video = np.array(output_video)
    print(output_video.shape)
    vwrite(filename, output_video,
           inputdict={'-r': '60'},
           outputdict={'-r': '60'})


def read_video(in_filename, frames_to_process):
    vidcap = cv2.VideoCapture(in_filename)
    success, image = vidcap.read()
    count = 0
    success = True
    while success and count < frames_to_process:
        yield image
        success, image = vidcap.read()
        count += 1


def find_biggest_face(face_locations):
    max_area = 0
    max_bound = face_locations[0]
    for face_bounds in face_locations:
        bottom_right_x = face_bounds[1]
        top_left_y = face_bounds[0]
        top_left_x = face_bounds[3]
        bottom_right_y = face_bounds[2]
        # w, h = abs(top_right_x - bottom_left_x), abs(top_right_y - bottom_left_y)

        w, h = bottom_right_x - top_left_x, bottom_right_y - top_left_y
        if w*h > max_area:
            max_area = w * h
            max_bound = face_bounds
    return max_bound


def find_face(image):
    face_locations = face_recognition.face_locations(image, number_of_times_to_upsample=0)  # model="cnn"
    if len(face_locations) > 0:
        H, W, _ = image.shape
        true_face_bounds = find_biggest_face(face_locations)

        # crop face from image with margin
        margin = 150

        face_y = true_face_bounds[0] - margin
        face_yh = true_face_bounds[2] + margin
        face_x = true_face_bounds[3] - margin
        face_xw = true_face_bounds[1] + margin

        # if face is close to the image border
        # image.shape => (height, width, channels)
        # face_y = true_face_bounds[0] if face_y < 0 else face_y
        # face_yh = true_face_bounds[2] if face_yh > image.shape[0] else face_yh
        # face_x = true_face_bounds[3] if face_x < 0 else face_x
        # face_xw = true_face_bounds[1] if face_xw > image.shape[1] else face_xw

        face_y = max(0, face_y)
        face_yh = min(face_yh, H)
        face_x = max(0, face_x)
        face_xw = min(face_xw, W)

        #face_cropped = image[face_y:face_yh, face_x:face_xw]
        return (face_y, face_yh), (face_x, face_xw)


def write_frames(images, out_dir):
    count = 0
    for image in images:
        img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        cv2.imwrite(os.path.join(out_dir, "frame{}.jpg".format(count)), img)  # save frame as JPEG file
        count += 1
    return count


def load_model(model, weights_path):
    weights = torch.load(weights_path, map_location=lambda storage, loc: storage)
    start_epoch = weights['epoch']
    best_acc = weights['best_acc']
    keys = weights['state_dict'].keys()
    new_dict = {}
    for old_key in keys:
        new_key = '.'.join(old_key.split('.')[1:])
        new_dict[new_key] = weights['state_dict'][old_key]

    model.load_state_dict(new_dict)
    return model
