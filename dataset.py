from __future__ import print_function, absolute_import

import os

import cv2
from skimage import io as skimio
import pandas as pd
import numpy as np

from torch.utils.data import Dataset
from albumentations import *

from utils.image_utils import im_to_torch, generate_hm


class LandmarkDataset(Dataset):
    def __init__(self, root_dir, out_res=(256, 256), train=True, transformation=None):
        self.root_dir = root_dir  # root dataset directory
        self.is_train = train  # training set or test set
        self.out_res = out_res

        annotations_file_path = os.path.join(self.root_dir, "annotations.csv")
        self.anno = pd.read_csv(annotations_file_path, sep=',', header=0)

        val_part = 0.1
        border = int(val_part * self.anno.shape[0])
        self.valid, self.train = list(range(border)), list(range(border, self.anno.shape[0]))

        if self.is_train:
            self.transform = Compose([
                OneOf([
                    IAAAdditiveGaussianNoise(),
                    GaussNoise(),
                ], p=0.6),
                HorizontalFlip(p=0.05),
                ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.2, rotate_limit=30, p=0.05),
                OneOf([
                    MotionBlur(p=.1),
                    MedianBlur(blur_limit=3, p=.3),
                    Blur(blur_limit=3, p=.3),
                ], p=0.6),
                OneOf([
                    OpticalDistortion(p=0.3),
                    GridDistortion(p=.1),
                    IAAPiecewiseAffine(p=0.3),
                ], p=0.3),
                OneOf([
                    CLAHE(clip_limit=2),
                    IAASharpen(),
                    IAAEmboss(),
                    RandomContrast(),
                    RandomBrightness(),
                    RandomGamma()
                ], p=0.6),
                HueSaturationValue(p=0.7),
                Normalize(
                    mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225],
                ),
            ])
        else:
            self.transform = Compose([
                Normalize(
                    mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225],
                ),
            ])

    def __len__(self):
        if self.is_train:
            return len(self.train)
        else:
            return len(self.valid)

    def __getitem__(self, index):
        if self.is_train:
            a = self.anno.iloc[self.train[index], :]
        else:
            a = self.anno.iloc[self.valid[index], :]

        landmarks = a.iloc[:-1]
        num_landmarks = landmarks.shape[0] // 2
        landmarks = landmarks.values.reshape((num_landmarks, 2))

        img_path = os.path.join(self.root_dir, a["image_path"])
        # image = cv2.imread(img_path)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = skimio.imread(img_path)

        H_in, W_in, _ = image.shape
        H_out, W_out = self.out_res
        ry = H_out / H_in
        rx = W_out / W_in

        image = cv2.resize(image, (W_out, H_out))
        landmarks *= np.array([rx, ry])

        # Generate ground truth
        target = np.zeros((H_out, W_out, num_landmarks))
        for i in range(num_landmarks):
            target[:, :, i] = generate_hm(H_out, W_out, landmarks[i])

        augmented = self.transform(image=image, mask=target)

        inp = im_to_torch(augmented['image'])
        target = im_to_torch(augmented['mask'])

        # Meta info
        meta = {'index': index, 'pts': landmarks.tolist(), 'img_path': img_path}

        return inp, target, meta
