#!/bin/bash

python train.py -m unet_resnet --stacks 15 --blocks 3 --name unet_resnet18_sigm --dataroot datasets/wflw_reduced/train_5k_val_3k/train --resnet-size 18
