import os
import errno
import time
from tqdm import tqdm

import torch
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim

from tensorboardX import SummaryWriter
from utils.misc import save_checkpoint, adjust_learning_rate

from torchvision import transforms, datasets

from models.mobilenet import MobileNetV2
from evaluation import AverageMeter
from options.train_options import TrainOptions


best_acc = 0
best_loss = 1000000.0


def main(args):
    global best_acc
    global best_loss

    # create resnet101 dir
    if not os.path.isdir(os.path.join("checkpoints", args.name)):
        try:
            os.makedirs(os.path.join("checkpoints", args.name))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    # create model
    model = MobileNetV2(n_class=10)
    model = torch.nn.DataParallel(model).cuda()

    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=args.lr)
    batch_size = 4

    # optionally resume from a resnet101
    title = 'keypoints_hg'
    if args.resume:
        if args.resume and os.path.isdir(os.path.join("checkpoints", args.name)):
            print("=> loading '{}'".format(args.name))
            path = os.path.join("checkpoints", args.name, "checkpoint.pth.tar")
            checkpoint = torch.load(path)
            args.start_epoch = checkpoint['epoch']
            best_acc = checkpoint['best_acc']
            model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded '{}' (epoch {})"
                  .format(args.name, checkpoint['epoch']))
        else:
            print("=> no model found at '{}'".format(args.resume))

    cudnn.benchmark = True
    print('    Total params: %.2fM' % (sum(p.numel() for p in model.parameters()) / 1000000.0))

    # Data loading code
    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    input_size = 32
    train_dataset = datasets.CIFAR10(
        root="data_cifar",
        train=True,
        download=True,
        transform = transforms.Compose([
            transforms.RandomResizedCrop(input_size, scale=(0.2, 1.0)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ]))

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(
            root="data_cifar",
            train=False,
            download=True,
            transform=transforms.Compose([
                transforms.Resize(int(input_size / 0.875)),
                transforms.CenterCrop(input_size),
                transforms.ToTensor(),
                normalize,
            ])),
        batch_size=batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    lr = args.lr

    save_path = "./logs/{}".format(args.name)
    logger = SummaryWriter(save_path)

    for epoch in range(args.start_epoch, args.epochs):
        lr = adjust_learning_rate(optimizer, epoch, lr, args.schedule, args.gamma)
        print('\nEpoch: %d | LR: %.8f' % (epoch + 1, lr))

        # decay sigma
        if args.sigma_decay > 0:
            train_loader.dataset.sigma *= args.sigma_decay
            val_loader.dataset.sigma *= args.sigma_decay

        # train for one epoch
        train_loss = train(train_loader, model, criterion, optimizer, logger, epoch, args.debug, args.flip)

        # evaluate on validation set
        valid_loss = validate(val_loader, model, criterion, args.num_classes, logger, epoch,
                              args.debug, args.flip)

        logger.add_scalar('train_loss', train_loss, epoch)
        logger.add_scalar('val_loss', valid_loss, epoch)

        # remember best acc and save resnet101
        #         is_best = valid_acc > best_acc
        #         best_acc = max(valid_acc, best_acc)
        is_best = valid_loss < best_loss
        best_loss = min(valid_loss, best_loss)
        save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_acc': best_loss,
            'optimizer': optimizer.state_dict(),
        }, is_best, checkpoint=os.path.join("checkpoints", args.name), snapshot=20)


def train(train_loader, model, criterion, optimizer, logger, epoch, debug=False, flip=True):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()

    gt_win, pred_win = None, None


    for i, (inputs, labels) in tqdm(enumerate(train_loader)):
        # measure data loading time
        data_time.update(time.time() - end)

        input_var = torch.autograd.Variable(inputs.cuda())
        target_var = torch.autograd.Variable(labels.cuda())

        # compute output
        output = model(input_var)
        score_map = output.data.cpu()

        # print(target_var)
        loss = criterion(output, target_var)

        # print(loss.item())

        # measure accuracy and record loss
        losses.update(loss.item(), inputs.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    return losses.avg


def validate(val_loader, model, criterion, num_classes, logger, epoch, debug=False, flip=True):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()

    # switch to evaluate mode
    model.eval()

    gt_win, pred_win = None, None
    end = time.time()

    for i, (inputs, labels) in enumerate(val_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        input_var = torch.autograd.Variable(inputs.cuda())
        target_var = torch.autograd.Variable(labels.cuda())

        # compute output
        with torch.no_grad():
            output = model(input_var)
            score_map = output.data.cpu()

        # if i == 0:
        #     disp_pred = inputs[0]
        #     for i in range(labels.shape[1]):
        #         disp_pred -= score_map[0][i]
        #     logger.add_image('Prediction', disp_pred, epoch)

        loss = criterion(output, target_var)

        # measure accuracy and record loss
        losses.update(loss.item(), inputs.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

    return losses.avg


if __name__ == '__main__':
    args = TrainOptions().parse()
    main(args)
