import argparse
import time
from imutils.video import VideoStream
import imutils
from albumentations import Compose, Normalize
from utils.utils import *
from detect import landmark_impainting
from models.unet_mobilenet import UNetMobileNet
from models.unet_resnet import UNetResNet

ap = argparse.ArgumentParser()

ap.add_argument("-v", "--video", type=str,
                help="path to input video file")
#ap.add_argument("-m", "--model", required=True, type=str,
#                help="Path to model")
ap.add_argument("-vb", "--verbose", action="store_true",
                help="If stated green sliding window is displayed")
args = vars(ap.parse_args())

if __name__ == "__main__":
    WEIGHTS_PATH = 'checkpoints/resnet18/model_best.pth.tar'
    VIDEO_PATH = "../videos/video_1.mov"
    detection_step = 200

    transform = Compose([
        Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225],
        ),
    ])

    #model = UNetMobileNet(56)
    model = UNetResNet(num_classes=56, encoder_depth=18)
    model = load_model(model, WEIGHTS_PATH)
    model.to('cuda')

    OPENCV_OBJECT_TRACKERS = {
            "csrt": cv2.TrackerCSRT_create,
            "kcf": cv2.TrackerKCF_create,
            "boosting": cv2.TrackerBoosting_create,
            "mil": cv2.TrackerMIL_create,
            "tld": cv2.TrackerTLD_create,
            "medianflow": cv2.TrackerMedianFlow_create,
            "mosse": cv2.TrackerMOSSE_create
        }

    tracker = OPENCV_OBJECT_TRACKERS["kcf"]()

    if not args.get("video", False):
        print("[INFO] starting video stream...")
        vs = VideoStream(src=0).start()
        time.sleep(1.0)

    # otherwise, grab a reference to the video file
    else:
        vs = cv2.VideoCapture(args["video"])

    face_detected = None
    initBB = None
    track = False
    counter = 0
    while True:
        # grab the current frame
        frame = vs.read()
        # handle the frame from VideoCapture or VideoStream
        frame = frame[1] if args.get("video", False) else frame

        # if we are viewing a video and we did not grab a frame,
        # then we have reached the end of the video
        if frame is None:
            break

        # resize the frame (so we can process it faster) and grab the
        # frame dimensions
        frame = imutils.resize(frame, width=400)
        (H, W) = frame.shape[:2]

        # update detection counter
        # if not track:
        #     counter += 1
        #     if counter < detection_step:
        #         cv2.imshow("Frame", frame)
        #         key = cv2.waitKey(1) & 0xFF
        #         continue
        #     else:
        #         counter = 0

        # check to see if we are currently tracking an object
        if initBB is not None:
            # grab the new bounding box coordinates of the object
            (success, box) = tracker.update(frame)

            # check to see if the tracking was a success
            if success:
                (x, y, w, h) = [int(v) for v in box]

                cv2.rectangle(frame, (x, y), (x + w, y + h),
                              (0, 255, 0), 2)

                start = time.time()
                frame = landmark_impainting(model, frame, face_detected, transform)
                #print(f"Landmark prediction took {time.time() - start}")

                # if x + w > W * track_box_offset_x or y + h > H * track_box_offset_y \
                #         or x < (1 - track_box_offset_x) * W or y < (1 - track_box_offset_y) * H:
                #     print("Tracker is not successful.")
                #     initBB = None
                #     palm_detected = False
                #     track = False
                #     continue
            else:
                print("Tracker is not successful.")
                initBB = None
                face_detected = False
                track = False

        if face_detected is None:
            start = time.time()
            face_detected = find_face(frame)
            print("Detection took", time.time() - start, "seconds")

        if face_detected is not None and not track:
            print("palm detected")
            # start track
            try:
                ((face_y, face_yh), (face_x, face_xw)) = face_detected

                initBB = (face_x, face_y, face_xw, face_yh)
                print("\n ===================== Track started ===============")
                track = True
                tracker.init(frame, initBB)

            except Exception as e:
                print("Exception =======", e)

        # show the frame to our screen
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        # if the 'q' key is pressed, stop the loop
        if key == ord("q"):
            break

        counter += 1




