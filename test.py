import numpy as np
import torch
import cv2
from models.unet_resnet import UNetResNet
from tqdm import tqdm
from dataset import LandmarkDataset
from options.test_options import TestOptions
from utils.utils import show_landmarks
from matplotlib import pyplot as plt


def test(args):
    weights = torch.load(args.weights, map_location=lambda storage, loc: storage)
    keys = weights['state_dict'].keys()
    new_dict = {}
    for old_key in keys:
        new_key = '.'.join(old_key.split('.')[1:])
        new_dict[new_key] = weights['state_dict'][old_key]

    model = UNetResNet(num_classes=args.num_classes)
    model.load_state_dict(new_dict)

    model.to('cpu')

    test_batch = 1
    loader = torch.utils.data.DataLoader(
        LandmarkDataset(args.dataroot, out_res=(args.load_size, args.load_size), train=False),
        batch_size=test_batch, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    for i, (inputs, orig, meta) in tqdm(enumerate(loader)):
        if i >= args.how_many:
            break

        img = cv2.imread(meta["img_path"][0], cv2.IMREAD_COLOR)

        input_var = torch.autograd.Variable(inputs.cpu(), volatile=True)
        output = model(input_var)
        score_map = output.data.cpu()

        # getting coordinates from heatmaps
        pts = np.zeros((model.num_classes, 2))
        for i in range(0, model.num_classes):
            point = np.array(np.unravel_index(score_map[0][i].argmax(), score_map[0][i].shape)[::-1])
            pts[i] = point

        show_landmarks(img, pts)
        plt.show()


if __name__ == "__main__":
    args = TestOptions().parse()
    test(args)
